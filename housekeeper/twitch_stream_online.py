import requests
import json


def is_live(streamer_name):
    client_id = open("twitch_client_id", "r")
    twitch_api_stream_url = "https://api.twitch.tv/kraken/streams/" \
                            + streamer_name + "?client_id=" + client_id.read()

    streamer_html = requests.get(twitch_api_stream_url)
    streamer = json.loads(streamer_html.content)

    return streamer["stream"]
