import discord
from discord.ext import commands
import random
import asyncio

import housekeeper.twitch_stream_online as twitch

description = '''An example bot to showcase the discord.ext.commands extension
module.
There are a number of utility commands being showcased here.'''
bot = commands.Bot(command_prefix='?', description=description)

twitch_streamer = []

async def check_twitch_stream_online():
    """
    Checks every 10 Seconds if a streamer is online
    :return:
    """
    await bot.wait_until_ready()
    channel = bot.get_channel(id='212974236912975872')
    while not bot.is_closed:
        tw_status = twitch.is_live("wiskeyyrl")
        if tw_status is not None:
            await bot.send_message(channel, "wiskeyyrl stated a stream on twitch")
        await asyncio.sleep(10)

@bot.event
async def on_ready():
    """
    Gets call when the bot is ready to use.
    Prints the name and the id of the bot
    """
    print("Name:", bot.user.name, "ID:", bot.user.id)
    print("Startup successfully")


token = open("token", "r")
bot.loop.create_task(check_twitch_stream_online())
bot.run(token.read())
